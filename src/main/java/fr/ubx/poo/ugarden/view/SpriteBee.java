package fr.ubx.poo.ugarden.view;

import fr.ubx.poo.ugarden.game.Direction;
import fr.ubx.poo.ugarden.go.GameObject;
import fr.ubx.poo.ugarden.go.personage.Bee;
import fr.ubx.poo.ugarden.go.personage.Player;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

public class SpriteBee extends Sprite {

    public SpriteBee(Pane layer,  Bee bee) {
        super(layer, null, bee);
        Image image = getImage();
    }
    private Image getImage() {
        return ImageResourceFactory.getInstance().getBee();
    }
}
