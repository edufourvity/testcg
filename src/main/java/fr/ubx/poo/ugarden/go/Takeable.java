/*
 * Copyright (c) 2020. Laurent Réveillère
 */

package fr.ubx.poo.ugarden.go;


import fr.ubx.poo.ugarden.go.bonus.Bonus;
import fr.ubx.poo.ugarden.go.personage.Player;

public interface Takeable {
    default void takenBy(Player player) {}

    default boolean poisonnedAppleIsTaken(){
        return false;
    }
}
