package fr.ubx.poo.ugarden.go.decor;

import fr.ubx.poo.ugarden.game.Game;
import fr.ubx.poo.ugarden.game.Position;
import fr.ubx.poo.ugarden.go.GameObject;
import fr.ubx.poo.ugarden.go.bonus.Bonus;
import fr.ubx.poo.ugarden.go.decor.Decor;
import fr.ubx.poo.ugarden.go.decor.ground.Ground;
import fr.ubx.poo.ugarden.go.personage.Player;

public class Princess extends Ground {

    public Princess(Position position) {
        super(position);
    }
    @java.lang.Override
    public boolean princessTaken() {
        return true;
    }
}
