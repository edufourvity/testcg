package fr.ubx.poo.ugarden.go;

import fr.ubx.poo.ugarden.go.bonus.*;
import fr.ubx.poo.ugarden.go.decor.Princess;

public interface TakeVisitor {

    // HealthInc
    default void takeHeart(Heart bonus) {
    }
    default void takeApple(Apple bonus) {
    }
    default void takePoisonedApple(PoisonedApple bonus) {
    }
}
