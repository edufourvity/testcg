/*
 * Copyright (c) 2020. Laurent Réveillère
 */

package fr.ubx.poo.ugarden.go.personage;

import fr.ubx.poo.ugarden.engine.Timer;
import fr.ubx.poo.ugarden.game.*;
import fr.ubx.poo.ugarden.go.GameObject;
import fr.ubx.poo.ugarden.go.Movable;
import fr.ubx.poo.ugarden.go.TakeVisitor;
import fr.ubx.poo.ugarden.go.WalkVisitor;
import fr.ubx.poo.ugarden.go.decor.Decor;
import fr.ubx.poo.ugarden.go.bonus.*;
import fr.ubx.poo.ugarden.go.decor.Princess;

import static fr.ubx.poo.ugarden.game.Direction.*;

public class Player extends GameObject implements Movable, TakeVisitor, WalkVisitor {

    private Direction direction;
    private boolean moveRequested = false;

    public Timer poisonedAppleTimer = new Timer(game.configuration().diseaseDuration());
    private int lives;
    public int getLives() {
        return lives;
    }
    private int energy;
    public int getEnergy() {
        return energy;
    }
    public void setPlayerEnergy(int energy) {
        this.energy=energy;
    }
    private int diseaseLevel;
    public int getDiseaseLevel() {
        return diseaseLevel;
    }
    public Direction getDirection() {
        return direction;
    }
    public boolean poisonedAppleTaken=false;

    public void requestMove(Direction direction) {
        if (direction != this.direction) {
            this.direction = direction;
            setModified(true);
        }
        moveRequested = true;
    }

    @Override
    public final boolean canMove(Direction direction) {
        //bloquer les mouvements du perso aux limites de la carte
        Position position = getPosition();
        int w = position.x();
        int h = position.y();
        if (direction == UP) {
            h--;
        } else if (direction == DOWN) {
            h++;
        } else if (direction == LEFT) {
            w--;
        } else if (direction == RIGHT) {
            w++;
        }
        Position nextposition = new Position(position.level(), w, h);
        if (h < 0 || h >= this.game.world().getGrid().height() || w < 0 || w >= this.game.world().getGrid().width()) {
            return false;
        }

        if (this.game.world().getGrid(this.game.world().currentLevel()).get(nextposition).walkableBy(this)==false){
            return false;
        }

        if (getEnergy()<=0){
            return false;
        }
        return true;
   }

   int numberPoisonedAppleTaken=0;

    public void update(long now) {

        poisonedAppleTimer.start();
        poisonedAppleTimer.update(now);
        if(!poisonedAppleTimer.isRunning() && poisonedAppleTimer.hasBeenReset() && numberPoisonedAppleTaken!=0){
            diseaseLevel-=1;
            numberPoisonedAppleTaken--;
        }
        if (moveRequested) {
            if (canMove(direction)) {
                doMove(direction);
                if (this.game.world().getGrid(this.game.world().currentLevel()).get(getPosition()).energyConsumptionWalk() == 1) {
                    setPlayerEnergy(this.energy - (1 * this.diseaseLevel));
                } else if (this.game.world().getGrid(this.game.world().currentLevel()).get(getPosition()).energyConsumptionWalk() == 2) {
                    setPlayerEnergy(this.energy - (2 * this.diseaseLevel));
                } else if (this.game.world().getGrid(this.game.world().currentLevel()).get(getPosition()).energyConsumptionWalk() == 3) {
                    setPlayerEnergy(this.energy - (3 * this.diseaseLevel));
                }
            }
        }

        moveRequested = false;
    }

    public Player(Game game, Position position) {
        super(game, position);
        this.direction = Direction.DOWN;
        this.lives = game.configuration().playerLives();
        this.energy=game.configuration().playerEnergy();
        this.diseaseLevel=game.configuration().diseaseLevel();
    }

    @Override
    public void takeHeart(Heart bonus) {
        // TODO
        this.lives+=1;
        System.out.println("I am taking the heart, I should do something ...");
    }

    public void takeApple(Apple bonus) {
        // TODO
        this.diseaseLevel=1;
        this.energy+=game.configuration().energyBoost();
        System.out.println("I am eating an apple, it was yummy !");
    }

    public void takePoisonedApple(PoisonedApple bonus) {
        // TODO
        this.diseaseLevel+=1;
        System.out.println("I am eating a poisoned apple, it was disgusting...");
        numberPoisonedAppleTaken++;
    }
    @Override
    public void doMove(Direction direction) {
        // This method is called only if the move is possible, do not check again
        Position nextPos = direction.nextPosition(getPosition());
        Decor next = game.world().getGrid().get(nextPos);
        setPosition(nextPos);
        if (next != null)
            next.takenBy(this);
    }

    @Override
    public String toString() {
        return "Player";
    }



}
